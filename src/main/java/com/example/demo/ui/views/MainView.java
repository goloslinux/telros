package com.example.demo.ui.views;

import com.example.demo.servise.UserService;
import com.example.demo.ui.views.Roles.ChangeRoleView;
import com.example.demo.ui.views.about.AboutView;
import com.example.demo.ui.views.user.AddUserView;
import com.example.demo.ui.views.user.DeleteUserView;
import com.example.demo.ui.views.user.UserView;
import com.vaadin.flow.component.applayout.AppLayout;
import com.vaadin.flow.component.applayout.DrawerToggle;
import com.vaadin.flow.component.html.Anchor;
import com.vaadin.flow.component.html.H1;
import com.vaadin.flow.component.orderedlayout.FlexComponent;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.HighlightConditions;
import com.vaadin.flow.router.Route;
import com.vaadin.flow.router.RouterLink;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

@Route("")
public class MainView extends AppLayout {

    private UserService userService;
    private RouterLink listLink = new RouterLink("About", AboutView.class);

    @Autowired
    public MainView() {

        createHeader();
        createDrawer();
    }

    private void createHeader() {
        H1 logo = new H1("Telros CRM");
        logo.addClassName("logo");
        listLink.setHighlightCondition(HighlightConditions.sameLocation());
        Anchor logout = new Anchor("/logout", "Log out");

        HorizontalLayout header = new HorizontalLayout(new DrawerToggle(), logo, logout);
        header.addClassName("header");
        header.setWidth("100%");
        header.expand(logo);
        header.setDefaultVerticalComponentAlignment(FlexComponent.Alignment.CENTER);

        addToNavbar(header);
    }

    private void createDrawer() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();

        boolean hasAdminRole = authentication.getAuthorities().stream()
                .anyMatch(r -> r.getAuthority().equals("ROLE_ADMIN"));

        VerticalLayout verticalLayout = new VerticalLayout(
                new RouterLink("Edit my profile", UserView.class)
        );

        if (hasAdminRole)
            verticalLayout.add(
                    new RouterLink("Add user", AddUserView.class),
                    new RouterLink("Delete users", DeleteUserView.class),
                    new RouterLink("Change role", ChangeRoleView.class),
                    listLink);
        verticalLayout.add(
                new Anchor("/logout", "Log out")
        );
        addToDrawer(verticalLayout);
    }
}
