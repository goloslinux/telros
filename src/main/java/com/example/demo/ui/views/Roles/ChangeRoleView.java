package com.example.demo.ui.views.Roles;

import com.example.demo.entity.Role;
import com.example.demo.entity.User;
import com.example.demo.servise.RoleService;
import com.example.demo.servise.UserService;
import com.example.demo.ui.views.MainView;
import com.vaadin.flow.component.combobox.ComboBox;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.html.NativeButton;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import java.util.Collections;
import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

@Route(value = "changeRole", layout = MainView.class)
@PageTitle("Add user")
public class ChangeRoleView extends Div {

    private UserService userService;
    private RoleService roleService;


    @Autowired
    public ChangeRoleView(UserService userService,
                          RoleService roleService) {
        setId("user-view");
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        this.userService = userService;
        AtomicReference<User> user = new AtomicReference<>(userService.findUserByUsername(authentication.getName()));
        this.roleService = roleService;


        List<User> userList = userService.allUsers();
        userList.remove(userService.findUserByUsername(authentication.getName()));
        AtomicReference<User> deletedUser = new AtomicReference<>(userList.get(0));

        ComboBox<User> selectUserBox = new ComboBox<>();
        selectUserBox.setLabel("User");
// Choose which property from Department is the presentation value
        selectUserBox.setItemLabelGenerator(User::getUsername);
        selectUserBox.setClearButtonVisible(true);
        selectUserBox.setLabel("Select user");
        selectUserBox.setItems(userList);

        List<Role> rolesList = roleService.allRoles();
        AtomicReference<Role> rolesdUser = new AtomicReference<>(rolesList.get(1));
        ComboBox<Role> selectRolesBox = new ComboBox<>();
        selectRolesBox.setLabel("Role");
// Choose which property from Department is the presentation value
        selectRolesBox.setItemLabelGenerator(Role::getName);
        selectRolesBox.setClearButtonVisible(true);
        selectRolesBox.setLabel("Select role");
        selectRolesBox.setItems(rolesList);

        Label infoLabel = new Label();
        NativeButton save = new NativeButton("Save");
        save.setEnabled(false);

        VerticalLayout verticalLayout = new VerticalLayout();
        HorizontalLayout actions = new HorizontalLayout();
        actions.add(selectUserBox);
        actions.add(selectRolesBox);
        verticalLayout.add(actions);
        verticalLayout.add(actions, save, infoLabel);
        add(verticalLayout);

        selectRolesBox.addValueChangeListener(event -> {
            if (event.getValue() == null) {
                save.setEnabled(false);
            } else {
                if (!selectUserBox.isEmpty()) save.setEnabled(true);
                rolesdUser.set(event.getValue());
            }
        });

        selectUserBox.addValueChangeListener(event -> {
            if (event.getValue() == null) {
                save.setEnabled(false);
            } else {
                if (!selectRolesBox.isEmpty()) save.setEnabled(true);
                user.set(event.getValue());
                ;
            }
        });

        save.addClickListener(event -> {
            user.get().setRoles(Collections.singleton(rolesdUser.get()));
            if (userService.resaveUser(user.get()))
                infoLabel.setText("Role saved");
            else
                infoLabel.setText("Role not saved");
        });
    }
}
