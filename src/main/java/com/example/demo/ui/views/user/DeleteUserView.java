package com.example.demo.ui.views.user;

import com.example.demo.entity.User;
import com.example.demo.servise.UserService;
import com.example.demo.ui.views.MainView;
import com.vaadin.flow.component.combobox.ComboBox;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.html.NativeButton;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

@Route(value = "deleteUser", layout = MainView.class)
@PageTitle("delete Users")
public class DeleteUserView extends Div {
    private UserService userService;

    @Autowired
    public DeleteUserView(UserService userService) {
        setId("delete-users");
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        this.userService = userService;

        List<User> userList = userService.allUsers();
        userList.remove(userService.findUserByUsername(authentication.getName()));
        AtomicReference<User> deletedUser = new AtomicReference<>(userList.get(0));

        ComboBox<User> selectUserBox = new ComboBox<>();
        selectUserBox.setLabel("User");
// Choose which property from Department is the presentation value
        selectUserBox.setItemLabelGenerator(User::getUsername);
        selectUserBox.setClearButtonVisible(true);
        selectUserBox.setLabel("Select user");
        selectUserBox.setItems(userList);

        Label infoLabel = new Label();
        NativeButton delete = new NativeButton("Delete");
        delete.setEnabled(false);

        VerticalLayout verticalLayout = new VerticalLayout();
        HorizontalLayout actions = new HorizontalLayout();
        actions.add(selectUserBox);
        verticalLayout.add(actions, delete, infoLabel);
        add(verticalLayout);

        selectUserBox.addValueChangeListener(event -> {
            if (event.getValue() == null) {
                delete.setEnabled(false);
            } else {
                delete.setEnabled(true);
                deletedUser.set(event.getValue())
                ;
            }
        });

        delete.addClickListener(event -> {
            if (userService.deleteUser(deletedUser.get().getId()))
                infoLabel.setText("User deleted");
            else
                infoLabel.setText("User not deleted");
        });
    }
}
