package com.example.demo.ui.views.user;


import com.example.demo.entity.User;
import com.example.demo.servise.UserService;
import com.example.demo.ui.views.MainView;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.html.NativeButton;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.data.binder.Binder;
import com.vaadin.flow.data.binder.BinderValidationStatus;
import com.vaadin.flow.data.binder.BindingValidationStatus;
import com.vaadin.flow.data.validator.EmailValidator;
import com.vaadin.flow.data.validator.StringLengthValidator;
import com.vaadin.flow.data.value.ValueChangeMode;
import com.vaadin.flow.function.SerializablePredicate;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import java.util.*;
import java.util.stream.Collectors;


@Route(value = "addUser", layout = MainView.class)
@PageTitle("Add user")
public class AddUserView extends Div {
    private UserService userService;

    @Autowired
    public AddUserView(UserService userService) {
        setId("user-view");
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        this.userService = userService;
        User user = new User();
        FormLayout layoutWithBinder = new FormLayout();
        Binder<User> binder = new Binder<>();
// Create the fields
        TextField login = new TextField();
        login.setValueChangeMode(ValueChangeMode.EAGER);
        login.setPlaceholder("Login");
        TextField password = new TextField();
        password.setValueChangeMode(ValueChangeMode.EAGER);
        password.setPlaceholder("Password");
        TextField firstName = new TextField();
        firstName.setValueChangeMode(ValueChangeMode.EAGER);
        firstName.setPlaceholder("Firstname");
        TextField middleName = new TextField();
        middleName.setValueChangeMode(ValueChangeMode.EAGER);
        middleName.setPlaceholder("Middlename");
        TextField lastName = new TextField();
        lastName.setValueChangeMode(ValueChangeMode.EAGER);
        lastName.setPlaceholder("Lastname");
        TextField phone = new TextField();
        phone.setPlaceholder("8121112233");
        phone.setValueChangeMode(ValueChangeMode.EAGER);
        TextField email = new TextField();
        email.setPlaceholder("user_login@exempl.ml");
        email.setValueChangeMode(ValueChangeMode.EAGER);
        Label infoLabel = new Label();
        NativeButton save = new NativeButton("Save");
        NativeButton reset = new NativeButton("Reset");
        layoutWithBinder.addFormItem(login, "Login");
        layoutWithBinder.addFormItem(password, "Password");
        layoutWithBinder.addFormItem(firstName, "First name");
        layoutWithBinder.addFormItem(middleName, "Middle name");
        layoutWithBinder.addFormItem(lastName, "Last name");
        layoutWithBinder.addFormItem(email, "E-mail");
        FormLayout.FormItem phoneItem = layoutWithBinder.addFormItem(phone, "Phone");
// Button bar
        HorizontalLayout actions = new HorizontalLayout();
        actions.add(save, reset);
        save.getStyle().set("marginRight", "10px");

        SerializablePredicate<String> phoneOrEmailPredicate = value -> !phone
                .getValue().trim().isEmpty()
                || !email.getValue().trim().isEmpty();

// E-mail and phone have specific validators
        Binder.Binding<User, String> emailBinding = binder.forField(email)
                .withValidator(phoneOrEmailPredicate,
                        "Both phone and email cannot be empty")
                .withValidator(new EmailValidator("Incorrect email address"))
                .bind(User::getEmail, User::setEmail);

        Binder.Binding<User, String> phoneBinding = binder.forField(phone)
                .withValidator(phoneOrEmailPredicate,
                        "Both phone and email cannot be empty")
                .bind(User::getPhone, User::setPhone);

// Trigger cross-field validation when the other field is changed
        email.addValueChangeListener(event -> phoneBinding.validate());
        phone.addValueChangeListener(event -> emailBinding.validate());

// First name and last name are required fields
        firstName.setRequiredIndicatorVisible(true);
        lastName.setRequiredIndicatorVisible(true);
        middleName.setRequiredIndicatorVisible(true);
        login.setRequiredIndicatorVisible(true);
        password.setRequiredIndicatorVisible(true);

        binder.forField(login)
                .withValidator(new StringLengthValidator(
                        "Please add the login", 5, null))
                .bind(User::getUsername, User::setUsername);
        binder.forField(password)
                .withValidator(new StringLengthValidator(
                        "Please add the password", 6, null))
                .bind(User::getPassword, User::setPassword);
        binder.forField(firstName)
                .withValidator(new StringLengthValidator(
                        "Please add the first name", 1, null))
                .bind(User::getFirstname, User::setFirstname);
        binder.forField(lastName)
                .withValidator(new StringLengthValidator(
                        "Please add the last name", 1, null))
                .bind(User::getLastname, User::setLastname);
        binder.forField(middleName)
                .withValidator(new StringLengthValidator(
                        "Please add the middle name", 1, null))
                .bind(User::getMiddlename, User::setMiddlename);


// Click listeners for the buttons
        save.addClickListener(event -> {
//            if (binder.writeBeanIfValid(userBeingEdited)) {
            if (binder.writeBeanIfValid(user)) {
                userService.saveUser(user);
                infoLabel.setText("Saved");
            } else {
                BinderValidationStatus<User> validate = binder.validate();
                String errorText = validate.getFieldValidationStatuses()
                        .stream().filter(BindingValidationStatus::isError)
                        .map(BindingValidationStatus::getMessage)
                        .map(Optional::get).distinct()
                        .collect(Collectors.joining(", "));
                infoLabel.setText("There are errors: " + errorText);
            }
        });
        reset.addClickListener(event -> {
            // clear fields by setting null
            binder.readBean(null);
            infoLabel.setText("");
        });
        add(layoutWithBinder, actions, infoLabel);
    }
}
