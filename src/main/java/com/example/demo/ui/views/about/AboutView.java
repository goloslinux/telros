package com.example.demo.ui.views.about;


import com.example.demo.entity.User;
import com.example.demo.repository.UserRepository;
import com.example.demo.servise.UserService;
import com.example.demo.ui.views.MainView;
import com.vaadin.flow.component.Text;
import com.vaadin.flow.component.html.Div;
import com.vaadin.flow.router.PageTitle;
import com.vaadin.flow.router.Route;
import org.springframework.beans.factory.annotation.Autowired;

@Route(value = "about", layout = MainView.class)
@PageTitle("About")
public class AboutView extends Div {

    @Autowired
    public AboutView() {
        setId("about-view");
        add(new Text(" Проект на базе фреймворков Spring boot, Spring security, Vaadin"));
        add(new Text(" Демонстрирует работу личного кабинета пользователя."));
        add(new Text(" У пользователя с ролью user есть доступ к страницам редактирвроания личных данных и \"about\"."));
        add(new Text(" У пользователя с ролью администратора есть доступ дополнительным к страницам создания и удаления пользователей"));
    }
}
