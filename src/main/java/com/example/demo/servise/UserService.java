package com.example.demo.servise;


import com.example.demo.entity.Role;
import com.example.demo.entity.User;
import com.example.demo.repository.RoleRepository;
import com.example.demo.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

@Service
public class UserService implements UserDetailsService {
    @Autowired
    UserRepository userRepository;
    @Autowired
    RoleRepository roleRepository;

    @Autowired
    BCryptPasswordEncoder bCryptPasswordEncoder;

    @PersistenceContext
    private EntityManager em;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = userRepository.findByUsername(username);

        if (user == null) {
            throw new UsernameNotFoundException("User not found");
        }

        return user;
    }

    public User findUserByUsername(String username) throws UsernameNotFoundException {

        User userFromDb = userRepository.findByUsername(username);
        System.out.println(userFromDb);


        if (userFromDb == null) {
            throw new UsernameNotFoundException("User not found");
        }

        return userFromDb;
    }

    public User findUserById(Long userId) {
        Optional<User> userFromDb = userRepository.findById(userId);
        return userFromDb.orElse(new User());
    }

    public List<User> allUsers() {
        return userRepository.findAll();
    }

    public boolean saveUser(User user) {
        User userFromDB = userRepository.findByUsername(user.getUsername());
        user.setRoles(Collections.singleton(new Role(1L, "ROLE_USER")));
        user.setPassword(
                bCryptPasswordEncoder.encode(user.getPassword()));
        userRepository.save(user);
        return true;
    }

    public boolean resaveUser(User user) {
        User userFromDB = userRepository.findByUsername(user.getUsername());

        if ((user.getFirstname() != null)
                && (!user.getFirstname().isEmpty())) {
            userFromDB.setFirstname(user.getFirstname());
        }


        if ((user.getMiddlename() != null)
                && (!user.getMiddlename().isEmpty())) {
            userFromDB.setMiddlename(user.getMiddlename());
        }


        if (user.getLastname() != null) {
            userFromDB.setLastname(user.getLastname());
        }


        if ((user.getEmail() != null)
                && (!user.getEmail().isEmpty())) {
            userFromDB.setEmail(user.getEmail());
        }


        if ((user.getPhone() != null)
                && (!user.getPhone().isEmpty())) {
            userFromDB.setPhone(user.getPhone());
        }

        if ((user.getRoles() != null)
                && (!user.getRoles().isEmpty())) {
            userFromDB.setRoles(user.getRoles());
        }

        userRepository.save(userFromDB);
        return true;
    }


    public boolean deleteUser(Long userId) {
        if (userRepository.findById(userId).isPresent()) {
            userRepository.deleteById(userId);
            return true;
        }
        return false;
    }

    public List<User> usergtList(Long idMin) {
        return em.createQuery("SELECT u FROM User u WHERE u.id > :paramID"
                , User.class)
                .setParameter("paramId", idMin).getResultList();

    }
}
